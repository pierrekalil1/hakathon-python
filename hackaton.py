hackathon_1 = ["Team Kenzie", "Team Ateliware", "Team VHSYS", "Team Mirum"]
hackathon_2 = ["Team Ateliware", "Team Kenzie", "Team VHSYS", "Team Mirum"]
hackathon_3 = ["Team Mirum", "Team Ateliware","Team VHSYS", "Team Kenzie"]

def get_score(team_name: str, teams: list):
    position = teams.index(team_name) + 1
    return "A " + team_name + " ficou classificada em " + str(position)


teste = get_score("Team VHSYS", hackathon_3)
